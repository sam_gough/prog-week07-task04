﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task04
{
    class Program
    {
        static void Main(string[] args)
        {
            calculate();
        }

        static void calculate(double a = 5, double b = 3)
        {
            Console.WriteLine($"The total of {a} + {b} = {a + b}");
            Console.WriteLine($"The total of {a} - {b} = {a - b}");
            Console.WriteLine($"The total of {a} / {b} = {a / b}");
            Console.WriteLine($"The total of {a} * {b} = {a * b}");
        }
    }
}
